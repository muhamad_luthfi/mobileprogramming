import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/luthfi.jpg')} />
                    </View>
                    <Text style={styles.name}>Muhamad Luthfi</Text>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program Studi  </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>Kelas  </Text>
                    <Text style ={styles.state}>Malam B</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram </Text>
                    <Text style ={styles.state}>@iamluthfii</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'white',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: -50,
        left: 90
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
    },
    class: {
        bottom:-30,
        left: 90
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 20
    },
    instagram:{
        bottom: -5,
        left : 90
    },
    ig: {
        fontWeight :'bold',
        fontSize: 20
    },
    state : {
        fontSize : 20,
        left : 150,
        bottom :25
    }
});